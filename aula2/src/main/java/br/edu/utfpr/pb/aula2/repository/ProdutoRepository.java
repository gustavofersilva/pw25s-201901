package br.edu.utfpr.pb.aula2.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.edu.utfpr.pb.aula2.model.Produto;


public interface ProdutoRepository extends JpaRepository <Produto, Long> {
	List <Produto> findByNomeContaining(String nome);
	List <Produto> findByNomeContainingOrDescricaoContaining(String nome, String descricao);
	List <Produto> findByValorGreaterThanEqual(Double valor);
	List <Produto> findByDataLancamentoBetween(LocalDate dataIni, LocalDate dataFim);
	@Query (value = "Select p FROM Produto p WHERE p.valor > :valor", nativeQuery = false)
	List<Produto> findByValorMaior(@Param("valor")Double valor);
	
	@Query(value = "SELECT p.nome, p.valor FROM produto as p " + " WHERE p.data_lancamento between :dataIni AND :dataFim ", nativeQuery = true)
	List<Object[]> findByDataLancamentoEntre(@Param("dataIni") LocalDate dataIni, @Param("dataFim") LocalDate dataFim);

	@Query(value = "SELECT p FROM Produto p WHERE p.nome like :valor or " + " p.descricao like :valor ", nativeQuery = false)
	List<Produto> findByNomeLikeOrDescricaoLike(@Param("valor") String valor);
}
