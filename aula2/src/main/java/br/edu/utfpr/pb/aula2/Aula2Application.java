package br.edu.utfpr.pb.aula2;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.edu.utfpr.pb.aula2.model.Categoria;
import br.edu.utfpr.pb.aula2.repository.CategoriaRepository;
import br.edu.utfpr.pb.aula2.repository.ProdutoRepository;

@SpringBootApplication
public class Aula2Application implements CommandLineRunner {
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(Aula2Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Categoria c = new Categoria ();
		c.setDescricao("Categoria Nova!");
		
		categoriaRepository.save(c);
		
		System.out.println("\n Lista de Categorias \n");
		categoriaRepository.findAll().forEach(
				categoria -> System.out.println(categoria));
		
		System.out.println("\n Lista de Categorias Descricao LIKE \n");
		categoriaRepository.findByDescricaoLikeOrderByDescricao("%e%").forEach(categoria -> System.out.println(categoria));
		
		System.out.println("\n Lista de Produtos DataLancamento Between \n");
		produtoRepository.findByDataLancamentoBetween(LocalDate.of(2010, 1, 1), LocalDate.of(2015, 12, 31));
	}

}
