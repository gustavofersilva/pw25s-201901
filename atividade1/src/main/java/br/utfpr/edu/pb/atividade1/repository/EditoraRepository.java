package br.utfpr.edu.pb.atividade1.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.utfpr.edu.pb.atividade1.model.Editora;

public interface EditoraRepository extends JpaRepository<Editora, Integer> {

}
