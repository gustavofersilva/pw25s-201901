package br.utfpr.edu.pb.atividade1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.utfpr.edu.pb.atividade1.model.Livro;

public interface LivroRepository extends JpaRepository<Livro, Integer> {

	List <Livro> findByTituloContainingOrderByAnoAsc(String titulo);
	List <Livro> findByAno(Integer ano);
	List <Livro> findByGeneroDescricaoOrderByAno(String descricao);
	List <Livro> findByISBN (Integer ISBN);
	
	
}
