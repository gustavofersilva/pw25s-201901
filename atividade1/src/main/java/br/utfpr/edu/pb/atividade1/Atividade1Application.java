package br.utfpr.edu.pb.atividade1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.utfpr.edu.pb.atividade1.model.Autor;
import br.utfpr.edu.pb.atividade1.model.Cidade;
import br.utfpr.edu.pb.atividade1.model.Editora;
import br.utfpr.edu.pb.atividade1.model.Genero;
import br.utfpr.edu.pb.atividade1.repository.AutorRepository;
import br.utfpr.edu.pb.atividade1.repository.CidadeRepository;
import br.utfpr.edu.pb.atividade1.repository.EditoraRepository;
import br.utfpr.edu.pb.atividade1.repository.GeneroRepository;
import br.utfpr.edu.pb.atividade1.repository.LivroRepository;

@SpringBootApplication
public class Atividade1Application implements CommandLineRunner{

	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private EditoraRepository editoraRepository;
	
	@Autowired
	private GeneroRepository generoRepository;
	
	@Autowired
	private LivroRepository livroRepository;
	
	
	public static void main(String[] args) {
		SpringApplication.run(Atividade1Application.class, args);
	}
	
	public void run(String... args) throws Exception {

//		Autor a = new Autor ();
//		a.setNome("Machado de Assis");
//		
//		Cidade c = new Cidade();
//		c.setNome("Rio de Janeiro");
//		
//		Editora e = new Editora();
//		e.setNome("Brasiliense");
//		
//		Genero g = new Genero();
//		g.setDescricao("Romance");
//		
//		autorRepository.save(a);
//		cidadeRepository.save(c);
//		editoraRepository.save(e);
//		generoRepository.save(g);
//		
//	System.out.println("\n Lista de Autores \n");
//		autorRepository.findAll().forEach(
//				autor -> System.out.println(autor));
//		
//		System.out.println("\n Lista de Cidades \n");
//		cidadeRepository.findAll().forEach(
//				cidade -> System.out.println(cidade));
		
		System.out.println("\n Lista de Livros Titulo LIKE \n");
		livroRepository.findByTituloContainingOrderByAnoAsc("%D%").forEach(livro -> System.out.println(livro));
		livroRepository.findByAno(1882).forEach(livro -> System.out.println(livro));
		livroRepository.findByGeneroDescricaoOrderByAno("Romance").forEach(livro -> System.out.println(livro));
	}
}
