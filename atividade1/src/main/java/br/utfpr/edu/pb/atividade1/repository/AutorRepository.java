package br.utfpr.edu.pb.atividade1.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.utfpr.edu.pb.atividade1.model.Autor;

public interface AutorRepository extends JpaRepository<Autor, Integer> {

}
