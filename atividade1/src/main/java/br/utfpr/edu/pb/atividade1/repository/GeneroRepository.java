package br.utfpr.edu.pb.atividade1.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.utfpr.edu.pb.atividade1.model.Genero;

public interface GeneroRepository extends JpaRepository<Genero, Integer>{

}
