package br.utfpr.edu.pb.atividade1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table (name = "livro")
@Data
@NoArgsConstructor
@EqualsAndHashCode (of = "id")
public class Livro {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(length = 100, nullable = false)
	private String titulo;
	
	@ManyToOne
	@JoinColumn(name = "editora_id", referencedColumnName = "id")
	private Editora editora;
	
	@ManyToOne
	@JoinColumn(name = "genero_id", referencedColumnName = "id")
	private Genero genero;
	
	@ManyToOne
	@JoinColumn(name = "autor_id", referencedColumnName = "id")
	private Autor autor;
	
	@Column(nullable = false)
	private Integer ano;
	
	@Column(nullable = false)
	private Integer isbn;
	
	@Column(nullable = false)
	private Double valor;

}
