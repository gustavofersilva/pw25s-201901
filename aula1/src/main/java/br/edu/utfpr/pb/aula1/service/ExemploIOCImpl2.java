package br.edu.utfpr.pb.aula1.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

public class ExemploIOCImpl2 {

	@Service("exemplo2")
	public class ExemploIOCImpl implements IExemploIOC{

		@Override
		public String getMensagem() {
			return "Mensagem do exemplo 2 !!";
		}
	}
}
