function saveJson(urlDestino) {
	var produtora = {
		id : ($("#id").val() != '' ? $("#id").val() : null),
		nome : $("#nome").val()
	}

	$.ajax({
		type : $('#frm').attr('method'),
		url : $('#frm').attr('action'),
		contentType : 'application/json',
		data : JSON.stringify(produtora),
		success : function() {
			swal('Salvo!', 'Registro salvo com sucesso!', 'success');
			window.location = urlDestino;
		},
		error : function() {
			swal('Erro!', 'Falha ao salvar registro!', 'error');
		}
	});// Fim ajax
}

function editProdutora(url) {
	$.get(url, function(entity, status) {
		$('#id').val(entity.id);
		$('#nome').val(entity.nome);
	});
	$('#modal-form').modal();
}