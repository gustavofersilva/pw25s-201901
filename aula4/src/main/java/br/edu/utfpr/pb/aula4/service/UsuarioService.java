package br.edu.utfpr.pb.aula4.service;

import br.edu.utfpr.pb.aula4.model.Usuario;

public interface UsuarioService extends CrudService<Usuario, Long> {

	Usuario findByUsername(String username);
	
}
