package br.edu.utfpr.pb.aula4.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.edu.utfpr.pb.aula4.report.SerieReport;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

@Controller
@RequestMapping("relatorio")
public class RelatorioController {
	
	@Autowired
	private SerieReport serieReport;
	
	@GetMapping ("serie/visualizar")
	public void serieVisualizar(HttpServletResponse response) throws SQLException, IOException, JRException  {
		JasperPrint jasperPrint = serieReport.generateReport("[ Relatorio de Series ]", 
				"classpath:/report/Series.jrxml",
				new BigDecimal(70));
		visualizar(jasperPrint, response);
	
	}
	
	public void visualizar (JasperPrint jasperPrint, HttpServletResponse response) throws IOException, JRException {
		response.setContentType("application/pdf");
		OutputStream out = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint, out);
	}
	
	public void baixar(JasperPrint jasperPrint, HttpServletResponse response) throws IOException, JRException {
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment;filename=\"RelatorioSerie.pdf\""));
		OutputStream out = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint, out);
	}
	
	

}
