package br.edu.utfpr.pb.aula4.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import br.edu.utfpr.pb.aula4.model.Serie;
import br.edu.utfpr.pb.aula4.service.SerieService;

@Controller
@RequestMapping("watch")
@SessionAttributes("watchList")
public class WatchListController {

	@Autowired
	private SerieService serieService;
	
	@ModelAttribute("watchList")
	private List<Serie> getWatchList() {
		return new ArrayList<>();
	}
	
	@GetMapping("add/{id}")
	public ResponseEntity<?> addSerie(@PathVariable Long id, Model model,
			@ModelAttribute("watchList") List<Serie> watchList) {
		
		watchList.add( serieService.findOne(id) );
		model.addAttribute("watchList", watchList);
		
		watchList.forEach(w -> System.out.println("Serie= " + w.getNome()));
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping("addv2/{id}")
	public ResponseEntity<?> addSerieV2(@PathVariable Long id, 
			HttpServletRequest request) {
		List<Serie> watchList;
		if (request.getSession().getAttribute("watchList") != null ) {
			watchList = (List<Serie>) request.getSession().getAttribute("watchList");
			watchList.add( serieService.findOne(id) );
		}else {
			watchList = new ArrayList<>();
			watchList.add( serieService.findOne(id) );
			request.getSession().setAttribute("watchList", watchList);
		}
			
		watchList.forEach(w -> System.out.println("Serie= " + w.getNome()));
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	
}
