package br.edu.utfpr.pb.aula4.config;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import br.edu.utfpr.pb.aula4.model.Usuario;
import br.edu.utfpr.pb.aula4.service.UsuarioService;

public class CrudAuditorAware implements AuditorAware<Usuario> {

	@Autowired
	private UsuarioService usuarioService;
	
	@Override
	public Optional<Usuario> getCurrentAuditor() {
		Authentication authentication = SecurityContextHolder
											.getContext()
											.getAuthentication();
	
		return Optional.ofNullable( 
				usuarioService.findByUsername(authentication.getName()));
	}

}
