package br.edu.utfpr.pb.aula4.service;

import br.edu.utfpr.pb.aula4.model.Serie;

public interface SerieService extends CrudService<Serie, Long> {

}
