package br.edu.utfpr.pb.aula4.service;

import br.edu.utfpr.pb.aula4.model.Produtora;

public interface ProdutoraService extends CrudService<Produtora, Integer> {

}
