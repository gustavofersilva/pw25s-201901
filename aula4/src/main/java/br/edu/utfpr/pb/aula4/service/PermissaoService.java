package br.edu.utfpr.pb.aula4.service;

import br.edu.utfpr.pb.aula4.model.Permissao;

public interface PermissaoService 
		extends CrudService<Permissao, Integer> {

}
