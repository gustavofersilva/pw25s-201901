package br.edu.utfpr.pb.aula4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import br.edu.utfpr.pb.aula4.config.CrudAuditorAware;
import br.edu.utfpr.pb.aula4.model.Usuario;
import nz.net.ultraq.thymeleaf.LayoutDialect;

@SpringBootApplication
@EnableJpaAuditing
public class Aula4Application {

	public static void main(String[] args) {
		SpringApplication.run(Aula4Application.class, args);
	}

	@Bean
	public LayoutDialect layoutDialect() {
	    return new LayoutDialect();
	}
	
	@Bean
	public AuditorAware<Usuario> auditorProvider() {
		return new CrudAuditorAware();
	}
}
