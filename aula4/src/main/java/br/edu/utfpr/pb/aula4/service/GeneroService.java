package br.edu.utfpr.pb.aula4.service;

import br.edu.utfpr.pb.aula4.model.Genero;

public interface GeneroService extends CrudService<Genero, Integer> {

}
