package br.edu.utfpr.pb.aula4.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.utfpr.pb.aula4.model.Produtora;

public interface ProdutoraRepository extends JpaRepository<Produtora, Integer>{

}
