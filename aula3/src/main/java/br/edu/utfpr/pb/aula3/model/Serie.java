package br.edu.utfpr.pb.aula3.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Entity
@Table(name = "serie")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@ToString
public class Serie implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 5194662911421051007L;
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nome", length = 100, nullable = false)
	private String nome;
	
	@Column(name = "resumo", length = 1000, nullable = false)
	private String resumo;
	
	@Column(name = "data_estreia", nullable = false)
	private LocalDate dataEstreia;
	
	@Column(name = "data_encerramento", nullable = true)
	private LocalDate dataEncerramento;
	
	@Column(name = "nota", nullable = false)
	private BigDecimal nota;
	
	@ManyToOne
	@JoinColumn (name = "produtora_id", referencedColumnName = "id")
	private Produtora produtora;
	
	@ManyToOne
	@JoinColumn (name = "genero_id", referencedColumnName = "id")
	private Genero genero;
	
	@Column(name = "imagem", length = 100, nullable = true)
	private String imagem;
	
	
}
