package br.edu.utfpr.pb.aula3.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.edu.utfpr.pb.aula3.model.Genero;
import br.edu.utfpr.pb.aula3.service.GeneroService;

@Controller
@RequestMapping("genero")
public class GeneroController {
	
	@Autowired
	private GeneroService generoService;
	
	@GetMapping()
	public String list(Model model) {
		model.addAttribute("generos",generoService.findAll());
		return "genero/list";
		
	}
	
	@GetMapping(value = {"new","novo"})
	public String form(Model model) {
		model.addAttribute("genero", new Genero());
		return "genero/form";
	}
	@PostMapping
	public String save(@Valid Genero genero, BindingResult result, Model model) {
		
		if (result.hasErrors()) {
			model.addAttribute("genero", genero);
			return "genero/form";
		}
		
		generoService.save(genero);
		model.addAttribute("gener"
				+ "os", generoService.findAll());
		return "genero/list";
		
	}
	
	@GetMapping("{id}")
	public String form(@PathVariable Integer id, Model model) {
		Genero genero = generoService.findOne(id);	
		
		
		model.addAttribute("genero", genero);
		
		return "genero/form";
	}
	
	@DeleteMapping("{id}")
	public String delete(@PathVariable("id") Integer id, Model model){
			
			generoService.delete(id);
	
			model.addAttribute("generos", generoService.findAll() );
			return "genero/list";
}
}
