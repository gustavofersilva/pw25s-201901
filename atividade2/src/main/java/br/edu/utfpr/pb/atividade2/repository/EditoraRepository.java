package br.edu.utfpr.pb.atividade2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.utfpr.pb.atividade2.model.Editora;

public interface EditoraRepository extends JpaRepository<Editora, Integer> {

}
